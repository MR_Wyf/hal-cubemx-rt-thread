/**
 ******************************************************************************
 * @file    rt_user_task.c
 * @brief   用户任务文件
 *
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 公众号：小飞哥玩嵌入式.
 * All rights reserved.
 * Author：小飞哥
 *
 *****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <rtthread.h>
#include "rt_user_task.h"
#include "button.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define THREAD1_PRIORITY  26    //线程
#define THREAD_STACK_SIZE 512  //线程栈深度
#define THREAD_TIMESLICE  5    //线程的时间片


#define THREAD2_PRIORITY 25    //线程

#define KEY_ON    0
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
Button_t Button1;
/* Private function prototypes -----------------------------------------------*/
static uint8_t rt_read_key1(void)
{
	return HAL_GPIO_ReadPin(USR_KEY1_GPIO_Port,USR_KEY1_Pin);
}

static void Btn1_Dowm_CallBack(void *btn)
{
  PRINT_INFO("Button1 单击!");
}

static void Btn1_Double_CallBack(void *btn)
{
  PRINT_INFO("Button1 双击!");
}

static void Btn1_Long_CallBack(void *btn)
{
  PRINT_INFO("Button1 长按!");
}

static void Btn1_Continuos_CallBack(void *btn)
{
  PRINT_INFO("Button1 连按!");
}
static void Btn1_ContinuosFree_CallBack(void *btn)
{
  PRINT_INFO("Button1 连按释放!");
}
/* Private user code ---------------------------------------------------------*/

/**
 * @function rt_ledflash_entry
 * @author:小飞哥玩嵌入式-小飞哥
 * @TODO:	LED控制线程
 * @param:
 * @return: NULL
 */
static void rt_led1_flash_entry(void *parameter)
{
	
	  Button_Create("Button1",
              &Button1, 
              rt_read_key1, 
              KEY_ON);
		Button_Attach(&Button1,BUTTON_DOWM,Btn1_Dowm_CallBack);                       //单击
		Button_Attach(&Button1,BUTTON_DOUBLE,Btn1_Double_CallBack);                   //双击
		Button_Attach(&Button1,BUTTON_CONTINUOS,Btn1_Continuos_CallBack);             //连按  
		Button_Attach(&Button1,BUTTON_CONTINUOS_FREE,Btn1_ContinuosFree_CallBack);    //连按释放  
		Button_Attach(&Button1,BUTTON_LONG,Btn1_Long_CallBack);        
		
   for (;;)
  {
		
		Button_Process();     //需要周期调用按键处理函数
		rt_thread_mdelay(20);
//		if(!HAL_GPIO_ReadPin(USR_KEY1_GPIO_Port,USR_KEY1_Pin))
//		{
//			rt_thread_mdelay(100);
//			if(!HAL_GPIO_ReadPin(USR_KEY1_GPIO_Port,USR_KEY1_Pin))//消抖
//			{
//				rt_kprintf("Key1 is presseded!\r\n");
//			}
//		}
//		if(!HAL_GPIO_ReadPin(USR_KEY2_GPIO_Port,USR_KEY2_Pin))
//		{
//			rt_thread_mdelay(100);
//			if(!HAL_GPIO_ReadPin(USR_KEY2_GPIO_Port,USR_KEY2_Pin))//消抖
//			{
//				rt_kprintf("Key2 is presseded!\r\n");
//			}
//		}
//		if(!HAL_GPIO_ReadPin(USR_KEY3_GPIO_Port,USR_KEY3_Pin))
//		{
//			rt_thread_mdelay(100);
//			if(!HAL_GPIO_ReadPin(USR_KEY3_GPIO_Port,USR_KEY3_Pin))//消抖
//			{
//				rt_kprintf("Key3 is presseded!\r\n");
//			}
//		}
  }
	
}
/**
 * @function rt_led2flash_entry
 * @author:小飞哥玩嵌入式-小飞哥
 * @TODO:	LED控制线程
 * @param:
 * @return: NULL
 */
static void rt_led2_flash_entry(void *parameter)
{
  for (;;)
  {
    HAL_GPIO_TogglePin(CP_LED_GPIO_Port, CP_LED_Pin);
//		rt_kprintf("LED2 Task is Running!\r\n");
    rt_thread_mdelay(500);
  }
}
/**
 * @function rt_user_thread_entry
 * @author:小飞哥玩嵌入式-小飞哥
 * @TODO:	创建线程
 * @param:
 * @return: NULL
 */

int rt_user_thread_entry(void)
{
  static rt_thread_t result = RT_NULL;
  /*创建一个线程，名称是rt_ledflash,入口是rt_ledflash_entry*/
  result = rt_thread_create("rt_led1flash", rt_led1_flash_entry,
                            NULL,
                            THREAD_STACK_SIZE,
                            THREAD1_PRIORITY,
                            THREAD_TIMESLICE);
  if (result != RT_NULL) //线程创建成功
	{
		rt_thread_startup(result);
	}
	else
	{
		rt_kprintf("\r\nrt_led1flash thread create failed\r\n");
	}
  /*创建一个线程，名称是rt_ledflash,入口是rt_ledflash_entry*/
  result = rt_thread_create("rt_led2flash", rt_led2_flash_entry,
                            NULL,
                            THREAD_STACK_SIZE,
                            THREAD2_PRIORITY,
                            THREAD_TIMESLICE);
  if (result != RT_NULL) //线程创建成功
	{
		rt_thread_startup(result);
	}
	else
	{
		rt_kprintf("\r\nrt_led2flash thread create failed\r\n");
	}
  return 0;
}
