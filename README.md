## 第一阶段

1. 基于Alios DevelopeKit开发板（主控STM32L496VGT6）;
2. HAL库与cubemx开发，RT_THread作为操作系统使用，仅仅是作为操作系统，目前不涉及软件包、驱动的使用;
3. 学会HAL库基本外设开发、cubemx使用，与RT-Thread操作系统使用;

## 第二阶段

1. 专注一些模组、小项目的开发使用,编写Alios DevelopeKit开发板板载传感器驱动;
2. 转入RT-Thread标准版，使用RT-Thread软件包、驱动库等开发,学习完整版RT_Thread;
3. 任重而道远，工作时间比较紧张，不定期更新，尽量做到周更，希望对大家有用，一起搞起来吧;

# 已发布HAL与cubemx与rt-thread教程

## RT_thread实战笔记

[RT_thread实战笔记](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzkyOTI4NjE0Mw==&action=getalbum&album_id=2064870121344499712&scene=173&from_msgid=2247488381&from_itemidx=1&count=3&nolastread=1#wechat_redirect)


## HAL与cubemx系列教程
[HAL与cubemx系列教程](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzkyOTI4NjE0Mw==&action=getalbum&album_id=2064863493941526528&scene=173&from_msgid=2247488557&from_itemidx=1&count=3&nolastread=1#wechat_redirect)

## 初学者适用

[别纠结了！一文搞懂HAL库是什么及如何使用](https://mp.weixin.qq.com/s/uJ8D0eptXffEYDzMin1KqQ)

[HAL库|神器cubemx的正确打开方式](https://mp.weixin.qq.com/s/KABGAQDDumDilBy74LBgaw)

[CubeMX与HAL库系列教程|点亮LED灯
](https://mp.weixin.qq.com/s/f8hHhMWzglcNsToMmVEr1Q)

[HAL库与Cubemx系列|Systick-系统滴答定时器详解](https://mp.weixin.qq.com/s/Msu7CH4ICN5H7cBxY5wr_A)

[HAL 库 uS 延时的 3 种实现方式](https://mp.weixin.qq.com/s/QzPojfnlK_M1eIvk8DXcOg)

# Alios DevelopeKit开发板介绍

之前拼夕夕上拼的开发板，价格相当可以，相信不少小伙伴都买了，小飞哥买来之后已经吃灰大半年了，这次也是重新拾起来，做教程学习使用，后续开发教程就在这块板子上进行啦

## 开发板简介

[硬件介绍传送门，biu~](https://github.com/alibaba/AliOS-Things/wiki/AliOS-Things-Developer-Kit-Hardware-Guide)
